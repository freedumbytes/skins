# Custom Maven Skins

[![Custom Maven Skins pipeline](https://freedumbytes.gitlab.io/setup/images/icon/gitlab.png "Custom Maven Skins pipeline")
 ![Custom Maven Skins pipeline](https://gitlab.com/freedumbytes/skins/badges/master/pipeline.svg "Custom Maven Skins pipeline")](https://gitlab.com/freedumbytes/skins)

[![Custom Maven Skins License](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Custom Maven Skins License")
 ![Custom Maven Skins License](https://img.shields.io/badge/Apache_License-2.0-orange.svg "Custom Maven Skins License")](https://www.apache.org/licenses/LICENSE-2.0.html)

[![Custom Maven Skins Maven Site](https://freedumbytes.gitlab.io/setup/images/icon/maven-v.png "Custom Maven Skins Maven Site")
 ![Custom Maven Skins Maven Site](https://img.shields.io/badge/Custom_Maven_Skins-latest_release-802C59 "Custom Maven Skins Maven Site")](https://freedumbytes.gitlab.io/skins)

 * [![Custom Maven Skins Reactor Dependency Convergence](https://img.shields.io/badge/Reactor_Dependency_Convergence-latest_release-802C59
      "Custom Maven Skins Reactor Dependency Convergence")](https://freedumbytes.gitlab.io/skins/dependency-convergence.html)
 * [![Custom Maven Skins Plugin Updates Report](https://img.shields.io/badge/Plugin_Updates_Report-latest_release-802C59
      "Custom Maven Skins Plugin Updates Report")](https://freedumbytes.gitlab.io/skins/plugin-updates-report.html)
 * [![Custom Maven Skins Property Updates Report](https://img.shields.io/badge/Property_Updates_Report-latest_release-802C59
      "Custom Maven Skins Property Updates Report")](https://freedumbytes.gitlab.io/skins/property-updates-report.html)
 * [![Custom Maven Skins Dependency Updates Report](https://img.shields.io/badge/Dependency_Updates_Report-latest_release-802C59
      "Custom Maven Skins Dependency Updates Report")](https://freedumbytes.gitlab.io/skins/dependency-updates-report.html)

[![Custom Maven Skins Javadoc](https://freedumbytes.gitlab.io/setup/images/icon/javadoc.png "Custom Maven Skins Javadoc")
 ![Custom Maven Skins Javadoc](https://img.shields.io/badge/Javadoc-latest_release-4D7A97 "Custom Maven Skins Javadoc")](https://freedumbytes.gitlab.io/skins/apidocs)

[![Custom Maven Skins Dependency Check](https://freedumbytes.gitlab.io/setup/images/icon/dependency-check.png "Custom Maven Skins Dependency Check")
 ![Custom Maven Skins Dependency Check](https://img.shields.io/badge/Dependency_Check-latest_release-F78D0A "Custom Maven Skins Dependency Check")](https://freedumbytes.gitlab.io/skins/dependency-check-report.html)

[![Custom Maven Skins Quality Gate Status](https://sonarcloud.io/api/project_badges/quality_gate?project=nl.demon.shadowland.freedumbytes.maven.custom.skins%3Afoundation "Custom Maven Skins Quality Gate Status")](https://sonarcloud.io/dashboard?id=nl.demon.shadowland.freedumbytes.maven.custom.skins%3Afoundation)

 * [![Custom Maven Skins Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=nl.demon.shadowland.freedumbytes.maven.custom.skins%3Afoundation&metric=reliability_rating
      "Custom Maven Skins Reliability Rating")](https://sonarcloud.io/component_measures?id=nl.demon.shadowland.freedumbytes.maven.custom.skins%3Afoundation&metric=reliability_rating&view=treemap)
   [![Custom Maven Skins Bugs](https://sonarcloud.io/api/project_badges/measure?project=nl.demon.shadowland.freedumbytes.maven.custom.skins%3Afoundation&metric=bugs
      "Custom Maven Skins Bugs")](https://sonarcloud.io/component_measures?id=nl.demon.shadowland.freedumbytes.maven.custom.skins%3Afoundation&metric=bugs)
 * [![Custom Maven Skins Security Rating](https://sonarcloud.io/api/project_badges/measure?project=nl.demon.shadowland.freedumbytes.maven.custom.skins%3Afoundation&metric=security_rating
      "Custom Maven Skins Security Rating")](https://sonarcloud.io/component_measures?id=nl.demon.shadowland.freedumbytes.maven.custom.skins%3Afoundation&metric=security_rating&view=treemap)
   [![Custom Maven Skins Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=nl.demon.shadowland.freedumbytes.maven.custom.skins%3Afoundation&metric=vulnerabilities
      "Custom Maven Skins Vulnerabilities")](https://sonarcloud.io/component_measures?id=nl.demon.shadowland.freedumbytes.maven.custom.skins%3Afoundation&metric=vulnerabilities)
 * [![Custom Maven Skins Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=nl.demon.shadowland.freedumbytes.maven.custom.skins%3Afoundation&metric=sqale_rating
      "Custom Maven Skins Maintainability Rating")](https://sonarcloud.io/component_measures?id=nl.demon.shadowland.freedumbytes.maven.custom.skins%3Afoundation&metric=sqale_rating&view=treemap)
   [![Custom Maven Skins Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=nl.demon.shadowland.freedumbytes.maven.custom.skins%3Afoundation&metric=sqale_index
      "Custom Maven Skins Technical Debt")](https://sonarcloud.io/component_measures?id=nl.demon.shadowland.freedumbytes.maven.custom.skins%3Afoundation&metric=sqale_index)
   [![Custom Maven Skins Code Smells](https://sonarcloud.io/api/project_badges/measure?project=nl.demon.shadowland.freedumbytes.maven.custom.skins%3Afoundation&metric=code_smells
      "Custom Maven Skins Code Smells")](https://sonarcloud.io/component_measures?id=nl.demon.shadowland.freedumbytes.maven.custom.skins%3Afoundation&metric=code_smells)
 * [![Custom Maven Skins Coverage](https://sonarcloud.io/api/project_badges/measure?project=nl.demon.shadowland.freedumbytes.maven.custom.skins%3Afoundation&metric=coverage
      "Custom Maven Skins Coverage")](https://sonarcloud.io/component_measures?id=nl.demon.shadowland.freedumbytes.maven.custom.skins%3Afoundation&metric=coverage&view=treemap)
 * [![Custom Maven Skins Duplicated Lines Density](https://sonarcloud.io/api/project_badges/measure?project=nl.demon.shadowland.freedumbytes.maven.custom.skins%3Afoundation&metric=duplicated_lines_density
      "Custom Maven Skins Duplicated Lines Density")](https://sonarcloud.io/component_measures?id=nl.demon.shadowland.freedumbytes.maven.custom.skins%3Afoundation&metric=duplicated_lines_density&view=treemap)
 * [![Custom Maven Skins Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=nl.demon.shadowland.freedumbytes.maven.custom.skins%3Afoundation&metric=ncloc
      "Custom Maven Skins Lines of Code")](https://sonarcloud.io/component_measures?id=nl.demon.shadowland.freedumbytes.maven.custom.skins%3Afoundation&metric=ncloc)

[![Custom Maven Skins Maven Central](https://freedumbytes.gitlab.io/setup/images/icon/central.png "Custom Maven Skins Maven Central")
 ![Custom Maven Skins Maven Central](https://img.shields.io/maven-central/v/nl.demon.shadowland.freedumbytes.maven.custom.skins/foundation.svg?label=Maven%20Central
   "Custom Maven Skins Maven Central")](https://search.maven.org/search?q=g%3Anl.demon.shadowland.freedumbytes.maven.custom.skins)

[![Custom Maven Skins Nexus](https://freedumbytes.gitlab.io/setup/images/icon/nexus.png "Custom Maven Skins Nexus")
 ![Custom Maven Skins Nexus](https://img.shields.io/nexus/r/https/oss.sonatype.org/nl.demon.shadowland.freedumbytes.maven.custom.skins/foundation.svg?label=Nexus
   "Custom Maven Skins Nexus")](https://oss.sonatype.org/#nexus-search;gav~nl.demon.shadowland.freedumbytes.maven.custom.skins~~~~)

[![Custom Maven Skins MvnRepository](https://freedumbytes.gitlab.io/setup/images/icon/mvnrepository.png "Custom Maven Skins MvnRepository")
 ![Custom Maven Skins MvnRepository](https://img.shields.io/nexus/r/https/oss.sonatype.org/nl.demon.shadowland.freedumbytes.maven.custom.skins/foundation.svg?label=MvnRepository
   "Custom Maven Skins MvnRepository")](https://mvnrepository.com/artifact/nl.demon.shadowland.freedumbytes.maven.custom.skins)

## Legend

 * [![Apache License](https://freedumbytes.gitlab.io/setup/images/icon/apache.png "Apache License") Apache License](https://apache.org/licenses) terms and conditions for use, reproduction, and distribution.
 * [![Maven Site](https://freedumbytes.gitlab.io/setup/images/icon/maven-v.png "Maven Site") Maven generated Site](https://maven.apache.org/plugins/maven-site-plugin) includes the project's reports that were configured in the POM.
 * [![Javadoc](https://freedumbytes.gitlab.io/setup/images/icon/javadoc.png "Javadoc") Javadoc](https://en.wikipedia.org/wiki/Javadoc) (originally cased JavaDoc) is a documentation generator created by Sun Microsystems for the Java language.
 * [![Dependency Check](https://freedumbytes.gitlab.io/setup/images/icon/dependency-check.png "Dependency Check") Dependency Check](https://owasp.org/www-project-dependency-check)
   is a utility that identifies project dependencies and checks if there are any known, publicly disclosed, vulnerabilities.
 * [![SonarCloud](https://freedumbytes.gitlab.io/setup/images/icon/sonarcloud.png "SonarCloud") SonarCloud](https://sonarcloud.io/explore/projects?sort=-analysis_date)
   inspects the quality of source code and detect bugs, vulnerabilities and code smells in more than 20 different languages.
 * [![Maven Central](https://freedumbytes.gitlab.io/setup/images/icon/central.png "Maven Central") The Central Repository](https://search.maven.org) official search.
 * [![Nexus](https://freedumbytes.gitlab.io/setup/images/icon/nexus.png "Nexus") Nexus Repository Manager](https://oss.sonatype.org) manages binaries and build artifacts across your software supply chain.
 * [![MvnRepository](https://freedumbytes.gitlab.io/setup/images/icon/mvnrepository.png "MvnRepository") MvnRepository](https://mvnrepository.com) is like Google for Maven artifacts with Dependencies Updates information.

## Table of Contents

[[_TOC_]]

## Available Documentation

Overall project documentation can be found at:

 * [![GitLab](https://freedumbytes.gitlab.io/setup/images/icon/gitlab.png "GitLab") freedumbytes.gitlab.io](https://freedumbytes.gitlab.io)

## Custom Maven Skins

To use for example the [![GitLab](https://freedumbytes.gitlab.io/setup/images/icon/maven-v.png "GitLab") Custom Reflow Skin](https://freedumbytes.gitlab.io/skins/maven-reflow-skin):

```xml
  <properties>
    <skinGroupId>nl.demon.shadowland.freedumbytes.maven.custom.skins</skinGroupId>
    <skinArtifactId>maven-reflow-skin</skinArtifactId>
    <skinVersion>${customMavenReflowSkinVersion}</skinVersion>

    <customSkinVersion>x.y.z</customSkinVersion>
    <customMavenPaperSkinVersion>${customSkinVersion}</customMavenPaperSkinVersion>
    <customMavenFluidoSkinVersion>${customSkinVersion}</customMavenFluidoSkinVersion>
    <customMavenReflowSkinVersion>${customSkinVersion}</customMavenReflowSkinVersion>
  </properties>

  <build>
    <pluginManagement>
      <plugins>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-site-plugin</artifactId>
          <version>${mavenSitePluginVersion}</version>
          <dependencies>
            <dependency>
              <groupId>nl.demon.shadowland.freedumbytes.patch.lt.velykis.maven.skins</groupId>
              <artifactId>reflow-velocity-tools</artifactId>
              <version>${customMavenReflowSkinVersion}</version>
            </dependency>
          </dependencies>
          <configuration>
            <generateSitemap>${generateSitemap}</generateSitemap>
            <inputEncoding>${project.build.sourceEncoding}</inputEncoding>
            <outputEncoding>${project.reporting.outputEncoding}</outputEncoding>
          </configuration>
        </plugin>
      </plugins>
    </pluginManagement>
  </build>
```

And the corresponding `site.xml`:

```xml
<?xml version="1.0" encoding="UTF-8"?>

<project xmlns="http://maven.apache.org/DECORATION/1.8.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/DECORATION/1.8.0 https://maven.apache.org/xsd/decoration-1.8.0.xsd"
         combine.self="override">
  <skin>
    <groupId>${skinGroupId}</groupId>
    <artifactId>${skinArtifactId}</artifactId>
    <version>${skinVersion}</version>
  </skin>

  <publishDate position="bottom" format="yyyy-MM-dd HH:mm:ss z" />
  <version position="navigation-bottom" />

  <custom>
    <reflowSkin>
      <localResources>true</localResources>

      <highlightJs>true</highlightJs>

      <topNav>parent|modules|reports</topNav>
      <navbarInverse>true</navbarInverse>

      <breadcrumbs>false</breadcrumbs>
      <toc>false</toc>

      <skinAttribution>false</skinAttribution>
    </reflowSkin>
  </custom>

  <body>
    <menu inherit="bottom" ref="parent" />
    <menu inherit="bottom" ref="modules" />
    <menu inherit="bottom" ref="reports" />
  </body>
</project>
```

### Maven Skins Overview

 * [maven-default-skin](https://maven.apache.org/skins/maven-default-skin) [1.3](https://github.com/apache/maven-default-skin/releases) ([changelog](https://issues.apache.org/jira/secure/ReleaseNote.jspa?projectId=12317926))
   [![Maven Default Skin Sample](https://img.shields.io/badge/Maven_Skin_Sample-Default-900 "Maven Default Skin Sample")](https://freedumbytes.gitlab.io/skins/maven-default-skin-sample)
 * ~~[maven-classic-skin](https://maven.apache.org/skins/maven-classic-skin)~~ 1.1 **retired** ([changelog](https://issues.apache.org/jira/secure/ReleaseNote.jspa?projectId=12317926))
   [![Maven Classic Skin Sample](https://img.shields.io/badge/Maven_Skin_Sample-Classic-036 "Maven Classic Skin Sample")](https://freedumbytes.gitlab.io/skins/maven-classic-skin-sample)
 * ~~[maven-stylus-skin](https://maven.apache.org/skins/maven-stylus-skin)~~ 1.5 **retired** ([changelog](https://issues.apache.org/jira/secure/ReleaseNote.jspa?projectId=12317926))
   [![Maven Stylus Skin Sample](https://img.shields.io/badge/Maven_Skin_Sample-Stylus-68A "Maven Stylus Skin Sample")](https://freedumbytes.gitlab.io/skins/maven-stylus-skin-sample)
 * ~~[maven-application-skin](https://maven.apache.org/skins/maven-application-skin)~~ 1.0 **retired** ([changelog](https://issues.apache.org/jira/secure/ReleaseNote.jspa?projectId=12317926))
   [![Maven Application Skin Sample](https://img.shields.io/badge/Maven_Skin_Sample-Application-F3B455 "Maven Application Skin Sample")](https://freedumbytes.gitlab.io/skins/maven-application-skin-sample)
 * [![GitLab](https://freedumbytes.gitlab.io/setup/images/icon/gitlab.png "GitLab") maven-paper-skin](https://gitlab.com/freedumbytes/skins/-/tree/master/maven-paper-skin) [1.4.1](https://gitlab.com/freedumbytes/skins/-/tags)
   [![Paper Skin Sample](https://img.shields.io/badge/Maven_Skin_Sample-Paper-E7E0CA "Paper Skin Sample")](https://freedumbytes.gitlab.io/skins/maven-paper-skin)
 * [maven-fluido-skin](https://maven.apache.org/skins/maven-fluido-skin) [1.10.0](https://github.com/apache/maven-fluido-skin/releases) ([changelog](https://issues.apache.org/jira/secure/ReleaseNote.jspa?projectId=12317926))
   [![Maven Fluido Skin](https://img.shields.io/badge/Maven_Skin-Fluido-08C "Maven Fluido Skin")](https://maven.apache.org/skins/maven-fluido-skin/sample.html)
 * Custom [![GitLab](https://freedumbytes.gitlab.io/setup/images/icon/gitlab.png "GitLab") maven-fluido-skin](https://gitlab.com/freedumbytes/skins/-/tree/master/maven-fluido-skin) [1.4.1](https://gitlab.com/freedumbytes/skins/-/tags)
   [![Custom Fluido Skin Sample](https://img.shields.io/badge/Custom_Skin-Fluido-08C "Custom Fluido Skin Sample")](https://freedumbytes.gitlab.io/skins/maven-fluido-skin) versus
   [![Original Fluido Skin Sample](https://img.shields.io/badge/Original_Skin-Fluido-08C "Original Fluido Skin Sample")](https://freedumbytes.gitlab.io/skins/maven-fluido-skin-sample)
 * [reflow-maven-skin](http://andriusvelykis.github.io/reflow-maven-skin) [1.1.1](https://github.com/andriusvelykis/reflow-maven-skin/releases) ([issues](https://github.com/andriusvelykis/reflow-maven-skin/milestones?state=closed))
   ([changelog](http://andriusvelykis.github.io/reflow-maven-skin/release-notes.html))
   [![Reflow Maven Skin](https://img.shields.io/badge/Maven_Skin-Reflow-225E9B "Reflow Maven Skin")](http://andriusvelykis.github.io/reflow-maven-skin/project-reports.html)
 * Custom [![GitLab](https://freedumbytes.gitlab.io/setup/images/icon/gitlab.png "GitLab") maven-reflow-skin](https://gitlab.com/freedumbytes/skins/-/tree/master/maven-reflow-skin) [1.4.1](https://gitlab.com/freedumbytes/skins/-/tags)
   [![Custom Reflow Skin Sample](https://img.shields.io/badge/Custom_Skin-Reflow-7F2B59 "Custom Reflow Skin Sample")](https://freedumbytes.gitlab.io/skins/maven-reflow-skin)
   with [![GitLab](https://freedumbytes.gitlab.io/setup/images/icon/gitlab.png "GitLab") reflow-velocity-tools-patch](https://gitlab.com/freedumbytes/skins/-/tree/master/reflow-velocity-tools-patch)
   for [![Maven Site Plugin 3.5 and higher](https://img.shields.io/badge/Maven_Site_Plugin-3.5_%2B-D5AF1B "Maven Site Plugin 3.5 and higher")](https://maven.apache.org/plugins/maven-site-plugin)
 * [docs-maven-skin](https://docs.bernardomg.com/maven/docs-maven-skin) [2.2.5](https://github.com/Bernardo-MG/docs-maven-skin/releases) ([changelog](https://docs.bernardomg.com/maven/docs-maven-skin/changes-report.html))
   [![Docs Maven Skin](https://img.shields.io/badge/Maven_Skin-Docs-FFF "Docs Maven Skin")](https://docs.bernardomg.com/maven/docs-maven-skin/reports.html)

The following JavaScript libraries are used by the Reflow Skin:

 * Build fast, responsive sites with [![Bootstrap](https://freedumbytes.gitlab.io/setup/images/icon/bootstrap.png "Bootstrap") Bootstrap](https://getbootstrap.com) ([changelog](https://getbootstrap.com/docs/versions))
 * [![Lightbox](https://freedumbytes.gitlab.io/setup/images/icon/lightbox.png "Lightbox") Lightbox](https://lokeshdhakar.com/projects/lightbox2) overlay images on top of the current page ([changelog](https://github.com/lokesh/lightbox2/releases)) at
   [cdnjs](https://cdnjs.com/libraries/lightbox2)
 * [![jQuery](https://freedumbytes.gitlab.io/setup/images/icon/jquery.png "jQuery") jQuery](https://jquery.com) a fast, small, and feature-rich JavaScript library ([changelog](https://code.jquery.com/jquery)) at
   [cdnjs](https://cdnjs.com/libraries/jquery)
   * [![jQuery Migrate](https://freedumbytes.gitlab.io/setup/images/icon/jquery.png "jQuery Migrate") jQuery Migrate](https://github.com/jquery/jquery-migrate)
     makes upgrading, by restoring the APIs that were removed, and additionally shows warnings in the browser console (development version of jQuery Migrate only) when removed and/or deprecated APIs are used
     ([changelog](https://github.com/jquery/jquery-migrate/tags)) at [cdnjs](https://cdnjs.com/libraries/jquery-migrate)
 * [![highlight.js](https://freedumbytes.gitlab.io/setup/images/icon/highlightjs.png "highlight.js") highlight.js](https://highlightjs.org) syntax highlighting and [styling](https://highlightjs.org/static/demo) for the web
   ([changelog](https://github.com/highlightjs/highlight.js/releases)) at [cdnjs](https://cdnjs.com/libraries/highlight.js)
 * [![HTML](https://freedumbytes.gitlab.io/setup/images/icon/html.png "HTML") The Story of the HTML5 Shiv](https://paulirish.com/2011/the-history-of-the-html5-shiv) ([changelog](https://github.com/aFarkas/html5shiv/releases)) at
   [cdnjs](https://cdnjs.com/libraries/html5shiv)

## Open Source Patches

[![GitHub](https://freedumbytes.gitlab.io/setup/images/icon/github.png "GitHub") Reflow Maven Skin](https://github.com/andriusvelykis/reflow-maven-skin):

 * [![issue 65](https://img.shields.io/github/issues/detail/state/andriusvelykis/reflow-maven-skin/65.svg "issue 65")](https://github.com/andriusvelykis/reflow-maven-skin/issues/65) Fix Html5 shiv URL HTTP `404`
   (see also [![pull request 62](https://img.shields.io/github/issues/detail/state/andriusvelykis/reflow-maven-skin/62.svg "pull request 62")](https://github.com/andriusvelykis/reflow-maven-skin/pull/62))
 * [![issue 66](https://img.shields.io/github/issues/detail/state/andriusvelykis/reflow-maven-skin/66.svg "issue 66")](https://github.com/andriusvelykis/reflow-maven-skin/issues/66)
   Fix `Back to top` link which isn't working because of smooth scrolling for anchor links
   (see also [![pull request 61](https://img.shields.io/github/issues/detail/state/andriusvelykis/reflow-maven-skin/61.svg "pull request 61")](https://github.com/andriusvelykis/reflow-maven-skin/pull/61))
 * [![issue 67](https://img.shields.io/github/issues/detail/state/andriusvelykis/reflow-maven-skin/67.svg "issue 67")](https://github.com/andriusvelykis/reflow-maven-skin/issues/67)
   Fix `nav` menu item double highlighting when last submenu item is selected
   (see also [![pull request 60](https://img.shields.io/github/issues/detail/state/andriusvelykis/reflow-maven-skin/60.svg "pull request 60")](https://github.com/andriusvelykis/reflow-maven-skin/pull/60))
 * [![issue 68](https://img.shields.io/github/issues/detail/state/andriusvelykis/reflow-maven-skin/68.svg "issue 68")](https://github.com/andriusvelykis/reflow-maven-skin/issues/68)
   Fix `Versions Plugin Updates Report` layout by disabling `fixTableHeads`
   (see also [![pull request 59](https://img.shields.io/github/issues/detail/state/andriusvelykis/reflow-maven-skin/59.svg "pull request 59")](https://github.com/andriusvelykis/reflow-maven-skin/pull/59))
 * [![Maven Site Plugin 3.5 and higher](https://img.shields.io/badge/Maven_Site_Plugin-3.5_%2B-D5AF1B "Maven Site Plugin 3.5 and higher")](https://maven.apache.org/plugins/maven-site-plugin)
   * [![issue 64](https://img.shields.io/github/issues/detail/state/andriusvelykis/reflow-maven-skin/64.svg "issue 64")](https://github.com/andriusvelykis/reflow-maven-skin/issues/64) Patch `reflow-velocity-tools`
     (see also [![pull request 63](https://img.shields.io/github/issues/detail/state/andriusvelykis/reflow-maven-skin/63.svg "pull request 63")](https://github.com/andriusvelykis/reflow-maven-skin/pull/63))
     * See also [![issue MSITE-782](https://img.shields.io/jira/issue/https/issues.apache.org/jira/MSITE-782.svg "issue MSITE-782")](https://issues.apache.org/jira/browse/MSITE-782)
       about `classpath:/META-INF/maven/site-tools.xml`
   * [![issue 69](https://img.shields.io/github/issues/detail/state/andriusvelykis/reflow-maven-skin/69.svg "issue 69")](https://github.com/andriusvelykis/reflow-maven-skin/issues/69)
     Fix `site.xml` `<head>` and `<footer>` after upgrade to `decoration-1.7.0.xsd`
     (see also [![pull request 58](https://img.shields.io/github/issues/detail/state/andriusvelykis/reflow-maven-skin/58.svg "pull request 58")](https://github.com/andriusvelykis/reflow-maven-skin/pull/58))
 * [![issue 70](https://img.shields.io/github/issues/detail/state/andriusvelykis/reflow-maven-skin/70.svg "issue 70")](https://github.com/andriusvelykis/reflow-maven-skin/issues/70)
   Hosting Maven site on `https` causes `Blocked loading mixed active content`
   (see also [![pull request 71](https://img.shields.io/github/issues/detail/state/andriusvelykis/reflow-maven-skin/71.svg "pull request 71")](https://github.com/andriusvelykis/reflow-maven-skin/pull/71))

> Aha, that is were _open_ stands for in **open source**. :grinning:
