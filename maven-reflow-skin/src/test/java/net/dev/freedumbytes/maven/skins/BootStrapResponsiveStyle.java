package net.dev.freedumbytes.maven.skins;


import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class BootStrapResponsiveStyle
{
  private static final Log logger = LogFactory.getLog(BootStrapResponsiveStyle.class);

  private static final String VAR_MIN_WIDTH = "<min-width>";
  private static final String VAR_GUTTER_WIDTH = "<gutter-width>";
  private static final String VAR_COLUMN_NUMBER = "<column-number>";
  private static final String VAR_WIDTH = "<width>";

  private static final String MEDIA = "@media(min-width:" + VAR_MIN_WIDTH + "px){.row{margin-left:-" + VAR_GUTTER_WIDTH
      + "px;*zoom:1}.row:before,.row:after{display:table;line-height:0;content:\"\"}.row:after{clear:both}[class*=\"span\"]{float:left;min-height:1px;margin-left:" + VAR_GUTTER_WIDTH + "px}";

  private static final String FIXED_GRID = ".container,.navbar-static-top .container,.navbar-fixed-top .container,.navbar-fixed-bottom .container{width:" + VAR_WIDTH + "px}";
  private static final String FIXED_GRID_WIDTH = ".span" + VAR_COLUMN_NUMBER + "{width:" + VAR_WIDTH + "px}";
  private static final String FIXED_GRID_OFFSET = ".offset" + VAR_COLUMN_NUMBER + "{margin-left:" + VAR_WIDTH + "px}";

  private static final int PERCENTAGE_PERUNAGE_FACTOR = new Integer(100);
  private static final String VAR_WIDTH_PCT = "<width-pct>";
  private static final String VAR_WIDTH_PADDING_PCT = "<width-padding-pct>";
  private static final String VAR_FIRST_WIDTH_PCT = "<first-width-pct>";
  private static final String VAR_FIRST_WIDTH_PADDING_PCT = "<first-width-padding-pct>";

  private static final String FLUID_GRID =
      ".row-fluid{width:100%;*zoom:1}.row-fluid:before,.row-fluid:after{display:table;line-height:0;content:\"\"}.row-fluid:after{clear:both}.row-fluid [class*=\"span\"]{display:block;float:left;width:100%;min-height:30px;margin-left:" + VAR_WIDTH_PCT
          + "%;*margin-left:" + VAR_WIDTH_PADDING_PCT
          + "%;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}.row-fluid [class*=\"span\"]:first-child{margin-left:0}.row-fluid .controls-row [class*=\"span\"]+[class*=\"span\"]{margin-left:" + VAR_WIDTH_PCT + "%}";
  private static final String FLUID_GRID_WIDTH = ".row-fluid .span" + VAR_COLUMN_NUMBER + "{width:" + VAR_WIDTH_PCT + "%;*width:" + VAR_WIDTH_PADDING_PCT + "%}";
  private static final String FLUID_GRID_OFFSET = ".row-fluid .offset" + VAR_COLUMN_NUMBER + "{margin-left:" + VAR_WIDTH_PCT + "%;*margin-left:" + VAR_WIDTH_PADDING_PCT + "%}.row-fluid .offset" + VAR_COLUMN_NUMBER + ":first-child{margin-left:"
      + VAR_FIRST_WIDTH_PCT + "%;*margin-left:" + VAR_FIRST_WIDTH_PADDING_PCT + "%}";

  private static final String INPUT_GRID = "input,textarea,.uneditable-input{margin-left:0}.controls-row [class*=\"span\"]+[class*=\"span\"]{margin-left:" + VAR_GUTTER_WIDTH + "px}";
  private static final String INPUT_GRID_WIDTH = "input.span" + VAR_COLUMN_NUMBER + ",textarea.span" + VAR_COLUMN_NUMBER + ",.uneditable-input.span" + VAR_COLUMN_NUMBER + "{width:" + VAR_WIDTH + "px}";

  private static final String THUMBNAIL = ".thumbnails{margin-left:-" + VAR_GUTTER_WIDTH + "px}.thumbnails>li{margin-left:" + VAR_GUTTER_WIDTH + "px}.row-fluid .thumbnails{margin-left:0}}";

  private static final MathContext mathContext = new MathContext(16, RoundingMode.HALF_UP);

  private static final int defaultRowWidth = 940;
  private static final BigDecimal defaultFluidPadding = new BigDecimal(PERCENTAGE_PERUNAGE_FACTOR).divide(new BigDecimal(defaultRowWidth), mathContext);
  private static final BigDecimal halfDefaultFluidPadding = defaultFluidPadding.divide(new BigDecimal(2), mathContext);
  private static final int defaultInputPadding = 14;

  private int gridColumns;
  private int gridColumnWidth;
  private int gridGutterWidth;
  private int gridRowWidth;
  private int minWidth;
  private BigDecimal gridColumnWidthPercentage;
  private BigDecimal gridGutterWidthPercentage;


  public BootStrapResponsiveStyle(int gridColumns, int gridColumnWidth, int gridGutterWidth)
  {
    this.gridColumns = gridColumns;
    this.gridColumnWidth = gridColumnWidth;
    this.gridGutterWidth = gridGutterWidth;

    gridRowWidth = (gridColumns * gridColumnWidth) + (gridGutterWidth * (gridColumns - 1));
    minWidth = gridRowWidth + gridGutterWidth;

    gridColumnWidthPercentage = new BigDecimal(PERCENTAGE_PERUNAGE_FACTOR * gridColumnWidth).divide(new BigDecimal(gridRowWidth), mathContext);
    logger.info(gridColumnWidthPercentage);

    gridGutterWidthPercentage = new BigDecimal(PERCENTAGE_PERUNAGE_FACTOR * gridGutterWidth).divide(new BigDecimal(gridRowWidth), mathContext);
    logger.info(gridGutterWidthPercentage);
  }


  protected String dumpCSS()
  {
    StringBuffer css = new StringBuffer();

    css.append(createMediaCSS());
    css.append(createFixedGrid());
    css.append(creatFluidGrid());
    css.append(createInputGrid());
    css.append(createThumbnailCSS());

    String customCSS = css.toString();
    logger.info(customCSS);

    return customCSS;
  }


  private String createMediaCSS()
  {
    return MEDIA.replaceAll(VAR_MIN_WIDTH, String.valueOf(minWidth)).replaceAll(VAR_GUTTER_WIDTH, String.valueOf(gridGutterWidth));
  }


  private String createFixedGrid()
  {
    int width = (gridColumns * gridColumnWidth) + ((gridColumns - 1) * gridGutterWidth);
    StringBuffer css = new StringBuffer();

    css.append(FIXED_GRID.replaceAll(VAR_WIDTH, String.valueOf(width)));

    for (int columnNumber = gridColumns; columnNumber > 0; columnNumber--)
    {
      width = (columnNumber * gridColumnWidth) + ((columnNumber - 1) * gridGutterWidth);

      css.append(FIXED_GRID_WIDTH.replaceAll(VAR_COLUMN_NUMBER, String.valueOf(columnNumber)).replaceAll(VAR_WIDTH, String.valueOf(width)));
    }

    for (int columnNumber = gridColumns; columnNumber > 0; columnNumber--)
    {
      width = (columnNumber * gridColumnWidth) + ((columnNumber + 1) * gridGutterWidth);

      css.append(FIXED_GRID_OFFSET.replaceAll(VAR_COLUMN_NUMBER, String.valueOf(columnNumber)).replaceAll(VAR_WIDTH, String.valueOf(width)));
    }

    return css.toString();
  }


  private String creatFluidGrid()
  {
    int width;
    BigDecimal widthPercentage;
    BigDecimal firstWidthPercentage;
    StringBuffer css = new StringBuffer();

    css.append(FLUID_GRID.replaceAll(VAR_WIDTH_PCT, String.valueOf(gridGutterWidthPercentage)).replaceAll(VAR_WIDTH_PADDING_PCT, String.valueOf(gridGutterWidthPercentage.subtract(halfDefaultFluidPadding))));

    for (int columnNumber = gridColumns; columnNumber > 0; columnNumber--)
    {
      width = (columnNumber * gridColumnWidth) + ((columnNumber - 1) * gridGutterWidth);
      widthPercentage = new BigDecimal(PERCENTAGE_PERUNAGE_FACTOR * width).divide(new BigDecimal(gridRowWidth), mathContext);

      css.append(FLUID_GRID_WIDTH.replaceAll(VAR_COLUMN_NUMBER, String.valueOf(columnNumber)).replaceAll(VAR_WIDTH_PCT, String.valueOf(widthPercentage)).replaceAll(VAR_WIDTH_PADDING_PCT,
          String.valueOf(widthPercentage.subtract(halfDefaultFluidPadding, mathContext))));
    }

    for (int columnNumber = gridColumns; columnNumber > 0; columnNumber--)
    {
      width = (columnNumber * gridColumnWidth) + ((columnNumber - 1) * gridGutterWidth);
      widthPercentage = new BigDecimal(PERCENTAGE_PERUNAGE_FACTOR * (width + 2 * gridGutterWidth)).divide(new BigDecimal(gridRowWidth), mathContext);
      firstWidthPercentage = new BigDecimal(PERCENTAGE_PERUNAGE_FACTOR * (width + gridGutterWidth)).divide(new BigDecimal(gridRowWidth), mathContext);

      css.append(FLUID_GRID_OFFSET.replaceAll(VAR_COLUMN_NUMBER, String.valueOf(columnNumber)).replaceAll(VAR_WIDTH_PCT, String.valueOf(widthPercentage)).replaceAll(VAR_WIDTH_PADDING_PCT, String.valueOf(widthPercentage.subtract(defaultFluidPadding)))
          .replaceAll(VAR_FIRST_WIDTH_PCT, String.valueOf(firstWidthPercentage)).replaceAll(VAR_FIRST_WIDTH_PADDING_PCT, String.valueOf(firstWidthPercentage.subtract(defaultFluidPadding))));
    }

    return css.toString();
  }


  private String createInputGrid()
  {
    int width;
    StringBuffer css = new StringBuffer();

    css.append(INPUT_GRID.replaceAll(VAR_GUTTER_WIDTH, String.valueOf(gridGutterWidth)));

    for (int columnNumber = gridColumns; columnNumber > 0; columnNumber--)
    {
      width = (columnNumber * gridColumnWidth) + ((columnNumber - 1) * gridGutterWidth) - defaultInputPadding;

      css.append(INPUT_GRID_WIDTH.replaceAll(VAR_COLUMN_NUMBER, String.valueOf(columnNumber)).replaceAll(VAR_WIDTH, String.valueOf(width)));
    }

    return css.toString();
  }


  private String createThumbnailCSS()
  {
    return THUMBNAIL.replaceAll(VAR_GUTTER_WIDTH, String.valueOf(gridGutterWidth));
  }
}
