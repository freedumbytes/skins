package net.dev.freedumbytes.maven.skins;


import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class BootStrapResponsiveStyleTest
{
  @Test()
  public void widthTablet()
  {
    assertThat(new BootStrapResponsiveStyle(12, 70, 30).dumpCSS())
        .isEqualTo("@media(min-width:1200px){.row{margin-left:-30px;*zoom:1}.row:before,.row:after{display:table;line-height:0;content:\"\"}.row:after{clear:both}[class*=\"span\"]{float:left;min-height:1px;margin-left:30px}"
            + ".container,.navbar-static-top .container,.navbar-fixed-top .container,.navbar-fixed-bottom .container{width:1170px}"
            + ".span12{width:1170px}.span11{width:1070px}.span10{width:970px}.span9{width:870px}.span8{width:770px}.span7{width:670px}.span6{width:570px}.span5{width:470px}.span4{width:370px}.span3{width:270px}.span2{width:170px}.span1{width:70px}"
            + ".offset12{margin-left:1230px}.offset11{margin-left:1130px}.offset10{margin-left:1030px}.offset9{margin-left:930px}.offset8{margin-left:830px}.offset7{margin-left:730px}.offset6{margin-left:630px}.offset5{margin-left:530px}.offset4{margin-left:430px}.offset3{margin-left:330px}.offset2{margin-left:230px}.offset1{margin-left:130px}"
            + ".row-fluid{width:100%;*zoom:1}.row-fluid:before,.row-fluid:after{display:table;line-height:0;content:\"\"}.row-fluid:after{clear:both}.row-fluid [class*=\"span\"]{display:block;float:left;width:100%;min-height:30px;margin-left:2.564102564102564%;*margin-left:2.51091107474086185%;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}.row-fluid [class*=\"span\"]:first-child{margin-left:0}.row-fluid .controls-row [class*=\"span\"]+[class*=\"span\"]{margin-left:2.564102564102564%}"
            + ".row-fluid .span12{width:100%;*width:99.94680851063830%}.row-fluid .span11{width:91.45299145299145%;*width:91.39979996362975%}.row-fluid .span10{width:82.90598290598291%;*width:82.85279141662121%}.row-fluid .span9{width:74.35897435897436%;*width:74.30578286961266%}.row-fluid .span8{width:65.81196581196581%;*width:65.75877432260411%}.row-fluid .span7{width:57.26495726495726%;*width:57.21176577559556%}"
            + ".row-fluid .span6{width:48.71794871794872%;*width:48.66475722858702%}.row-fluid .span5{width:40.17094017094017%;*width:40.11774868157847%}.row-fluid .span4{width:31.62393162393162%;*width:31.57074013456992%}.row-fluid .span3{width:23.07692307692308%;*width:23.02373158756138%}.row-fluid .span2{width:14.52991452991453%;*width:14.47672304055283%}.row-fluid .span1{width:5.982905982905983%;*width:5.929714493544281%}"
            + ".row-fluid .offset12{margin-left:105.1282051282051%;*margin-left:105.0218221494816957%}.row-fluid .offset12:first-child{margin-left:102.5641025641026%;*margin-left:102.4577195853791957%}.row-fluid .offset11{margin-left:96.58119658119658%;*margin-left:96.4748136024731757%}.row-fluid .offset11:first-child{margin-left:94.01709401709402%;*margin-left:93.9107110383706157%}"
            + ".row-fluid .offset10{margin-left:88.03418803418803%;*margin-left:87.9278050554646257%}.row-fluid .offset10:first-child{margin-left:85.47008547008547%;*margin-left:85.3637024913620657%}.row-fluid .offset9{margin-left:79.48717948717949%;*margin-left:79.3807965084560857%}.row-fluid .offset9:first-child{margin-left:76.92307692307692%;*margin-left:76.8166939443535157%}"
            + ".row-fluid .offset8{margin-left:70.94017094017094%;*margin-left:70.8337879614475357%}.row-fluid .offset8:first-child{margin-left:68.37606837606838%;*margin-left:68.2696853973449757%}.row-fluid .offset7{margin-left:62.39316239316239%;*margin-left:62.2867794144389857%}.row-fluid .offset7:first-child{margin-left:59.82905982905983%;*margin-left:59.7226768503364257%}"
            + ".row-fluid .offset6{margin-left:53.84615384615385%;*margin-left:53.7397708674304457%}.row-fluid .offset6:first-child{margin-left:51.28205128205128%;*margin-left:51.1756683033278757%}.row-fluid .offset5{margin-left:45.29914529914530%;*margin-left:45.1927623204218957%}.row-fluid .offset5:first-child{margin-left:42.73504273504274%;*margin-left:42.6286597563193357%}"
            + ".row-fluid .offset4{margin-left:36.75213675213675%;*margin-left:36.6457537734133457%}.row-fluid .offset4:first-child{margin-left:34.18803418803419%;*margin-left:34.0816512093107857%}.row-fluid .offset3{margin-left:28.20512820512821%;*margin-left:28.0987452264048057%}.row-fluid .offset3:first-child{margin-left:25.64102564102564%;*margin-left:25.5346426623022357%}"
            + ".row-fluid .offset2{margin-left:19.65811965811966%;*margin-left:19.5517366793962557%}.row-fluid .offset2:first-child{margin-left:17.09401709401709%;*margin-left:16.9876341152936857%}.row-fluid .offset1{margin-left:11.11111111111111%;*margin-left:11.0047281323877057%}.row-fluid .offset1:first-child{margin-left:8.547008547008547%;*margin-left:8.4406255682851427%}"
            + "input,textarea,.uneditable-input{margin-left:0}.controls-row [class*=\"span\"]+[class*=\"span\"]{margin-left:30px}" + "input.span12,textarea.span12,.uneditable-input.span12{width:1156px}"
            + "input.span11,textarea.span11,.uneditable-input.span11{width:1056px}" + "input.span10,textarea.span10,.uneditable-input.span10{width:956px}" + "input.span9,textarea.span9,.uneditable-input.span9{width:856px}"
            + "input.span8,textarea.span8,.uneditable-input.span8{width:756px}" + "input.span7,textarea.span7,.uneditable-input.span7{width:656px}" + "input.span6,textarea.span6,.uneditable-input.span6{width:556px}"
            + "input.span5,textarea.span5,.uneditable-input.span5{width:456px}" + "input.span4,textarea.span4,.uneditable-input.span4{width:356px}" + "input.span3,textarea.span3,.uneditable-input.span3{width:256px}"
            + "input.span2,textarea.span2,.uneditable-input.span2{width:156px}" + "input.span1,textarea.span1,.uneditable-input.span1{width:56px}" + ".thumbnails{margin-left:-30px}.thumbnails>li{margin-left:30px}.row-fluid .thumbnails{margin-left:0}}");
  }


  @Test()
  public void widthDesktop()
  {
    assertThat(new BootStrapResponsiveStyle(12, 112, 48).dumpCSS())
        .isEqualTo("@media(min-width:1920px){.row{margin-left:-48px;*zoom:1}.row:before,.row:after{display:table;line-height:0;content:\"\"}.row:after{clear:both}[class*=\"span\"]{float:left;min-height:1px;margin-left:48px}"
            + ".container,.navbar-static-top .container,.navbar-fixed-top .container,.navbar-fixed-bottom .container{width:1872px}"
            + ".span12{width:1872px}.span11{width:1712px}.span10{width:1552px}.span9{width:1392px}.span8{width:1232px}.span7{width:1072px}.span6{width:912px}.span5{width:752px}.span4{width:592px}.span3{width:432px}.span2{width:272px}.span1{width:112px}"
            + ".offset12{margin-left:1968px}.offset11{margin-left:1808px}.offset10{margin-left:1648px}.offset9{margin-left:1488px}.offset8{margin-left:1328px}.offset7{margin-left:1168px}.offset6{margin-left:1008px}.offset5{margin-left:848px}.offset4{margin-left:688px}.offset3{margin-left:528px}.offset2{margin-left:368px}.offset1{margin-left:208px}"
            + ".row-fluid{width:100%;*zoom:1}.row-fluid:before,.row-fluid:after{display:table;line-height:0;content:\"\"}.row-fluid:after{clear:both}.row-fluid [class*=\"span\"]{display:block;float:left;width:100%;min-height:30px;margin-left:2.564102564102564%;*margin-left:2.51091107474086185%;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}.row-fluid [class*=\"span\"]:first-child{margin-left:0}.row-fluid .controls-row [class*=\"span\"]+[class*=\"span\"]{margin-left:2.564102564102564%}"
            + ".row-fluid .span12{width:100%;*width:99.94680851063830%}.row-fluid .span11{width:91.45299145299145%;*width:91.39979996362975%}.row-fluid .span10{width:82.90598290598291%;*width:82.85279141662121%}.row-fluid .span9{width:74.35897435897436%;*width:74.30578286961266%}.row-fluid .span8{width:65.81196581196581%;*width:65.75877432260411%}.row-fluid .span7{width:57.26495726495726%;*width:57.21176577559556%}"
            + ".row-fluid .span6{width:48.71794871794872%;*width:48.66475722858702%}.row-fluid .span5{width:40.17094017094017%;*width:40.11774868157847%}.row-fluid .span4{width:31.62393162393162%;*width:31.57074013456992%}.row-fluid .span3{width:23.07692307692308%;*width:23.02373158756138%}.row-fluid .span2{width:14.52991452991453%;*width:14.47672304055283%}.row-fluid .span1{width:5.982905982905983%;*width:5.929714493544281%}"
            + ".row-fluid .offset12{margin-left:105.1282051282051%;*margin-left:105.0218221494816957%}.row-fluid .offset12:first-child{margin-left:102.5641025641026%;*margin-left:102.4577195853791957%}.row-fluid .offset11{margin-left:96.58119658119658%;*margin-left:96.4748136024731757%}.row-fluid .offset11:first-child{margin-left:94.01709401709402%;*margin-left:93.9107110383706157%}"
            + ".row-fluid .offset10{margin-left:88.03418803418803%;*margin-left:87.9278050554646257%}.row-fluid .offset10:first-child{margin-left:85.47008547008547%;*margin-left:85.3637024913620657%}.row-fluid .offset9{margin-left:79.48717948717949%;*margin-left:79.3807965084560857%}.row-fluid .offset9:first-child{margin-left:76.92307692307692%;*margin-left:76.8166939443535157%}"
            + ".row-fluid .offset8{margin-left:70.94017094017094%;*margin-left:70.8337879614475357%}.row-fluid .offset8:first-child{margin-left:68.37606837606838%;*margin-left:68.2696853973449757%}.row-fluid .offset7{margin-left:62.39316239316239%;*margin-left:62.2867794144389857%}.row-fluid .offset7:first-child{margin-left:59.82905982905983%;*margin-left:59.7226768503364257%}"
            + ".row-fluid .offset6{margin-left:53.84615384615385%;*margin-left:53.7397708674304457%}.row-fluid .offset6:first-child{margin-left:51.28205128205128%;*margin-left:51.1756683033278757%}.row-fluid .offset5{margin-left:45.29914529914530%;*margin-left:45.1927623204218957%}.row-fluid .offset5:first-child{margin-left:42.73504273504274%;*margin-left:42.6286597563193357%}"
            + ".row-fluid .offset4{margin-left:36.75213675213675%;*margin-left:36.6457537734133457%}.row-fluid .offset4:first-child{margin-left:34.18803418803419%;*margin-left:34.0816512093107857%}.row-fluid .offset3{margin-left:28.20512820512821%;*margin-left:28.0987452264048057%}.row-fluid .offset3:first-child{margin-left:25.64102564102564%;*margin-left:25.5346426623022357%}"
            + ".row-fluid .offset2{margin-left:19.65811965811966%;*margin-left:19.5517366793962557%}.row-fluid .offset2:first-child{margin-left:17.09401709401709%;*margin-left:16.9876341152936857%}.row-fluid .offset1{margin-left:11.11111111111111%;*margin-left:11.0047281323877057%}.row-fluid .offset1:first-child{margin-left:8.547008547008547%;*margin-left:8.4406255682851427%}"
            + "input,textarea,.uneditable-input{margin-left:0}.controls-row [class*=\"span\"]+[class*=\"span\"]{margin-left:48px}" + "input.span12,textarea.span12,.uneditable-input.span12{width:1858px}"
            + "input.span11,textarea.span11,.uneditable-input.span11{width:1698px}" + "input.span10,textarea.span10,.uneditable-input.span10{width:1538px}" + "input.span9,textarea.span9,.uneditable-input.span9{width:1378px}"
            + "input.span8,textarea.span8,.uneditable-input.span8{width:1218px}" + "input.span7,textarea.span7,.uneditable-input.span7{width:1058px}" + "input.span6,textarea.span6,.uneditable-input.span6{width:898px}"
            + "input.span5,textarea.span5,.uneditable-input.span5{width:738px}" + "input.span4,textarea.span4,.uneditable-input.span4{width:578px}" + "input.span3,textarea.span3,.uneditable-input.span3{width:418px}"
            + "input.span2,textarea.span2,.uneditable-input.span2{width:258px}" + "input.span1,textarea.span1,.uneditable-input.span1{width:98px}" + ".thumbnails{margin-left:-48px}.thumbnails>li{margin-left:48px}.row-fluid .thumbnails{margin-left:0}}");
  }
}
